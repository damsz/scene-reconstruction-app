import glob
import os
import time
from configparser import ConfigParser

import cv2
import numpy as np

config = ConfigParser()
config.read("source/config.ini")
image_mask = 'calibPhoto'

pattern_size = (7, 7)
square_size = 2.0

class CameraCalibration:

    def __init__(self, takePictures,cameraNo):
        self.cap = cv2.VideoCapture(cameraNo)
        self.noOfTakes = int(config["CALIBRATION"]["numberOfSamples"])

        if (takePictures):
            self.takeShots()

    def takeShots(self):
        iterator = 0
        print("Place Chess Board in diffrent positions in front of camera, when program will detect corners it will save picture.\n To exit type q \n To force saving picture type n")
        while (self.noOfTakes > iterator):
            ret, frame = self.cap.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            ret, corners = cv2.findChessboardCorners(gray, (7, 7), None)
            cv2.imshow('Calibration', gray)
            if ret == True:
                iterator += 1
                print('Taking next Calibration Picture ' + str(iterator) + ' change position of chessboard')
                cv2.imwrite('source/calib/calibPhoto_' + str(iterator) + '_' + time.strftime("%Y%m%d-%H%M%S") + '.jpg',
                            gray)
                time.sleep(1)
            key = cv2.waitKey(1) & 0xFF
            if key == ord('q'):
                self.cap.release()
                cv2.destroyAllWindows()
                break
            elif key == ord('n'):
                iterator += 1
                print('Taking next Calibration Picture ' + str(iterator))
                cv2.imwrite('source/calib/calibPhoto_' + str(iterator) + '_' + time.strftime("%Y%m%d-%H%M%S") + '.jpg',
                            gray)
        self.cap.release()
        cv2.destroyAllWindows()
        return

    def calibrate(self):
        img_names = glob.glob('source/calib/calibPhoto*')
       # img_names = glob.glob('source/calib2/*')
        addAll = False

        # termination criteria
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        objp = np.zeros((7 * 7, 3), np.float32)
        objp[:, :2] = np.mgrid[0:7, 0:7].T.reshape(-1, 2)

        # Arrays to store object points and image points from all the images.
        objpoints = []  # 3d point in real world space
        imgpoints = []  # 2d points in image plane.
        img = cv2.imread(img_names[0])
        h, w = img.shape[:2]
        target_width = 800
        for fname in img_names:
            img = cv2.imread(fname)
            while img.shape[1] > 2 * target_width:
                img = cv2.pyrDown(img)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            print("taking file " + str(fname))
            ret, corners = cv2.findChessboardCorners(
                gray, (7, 7),
                cv2.CALIB_CB_ADAPTIVE_THRESH
                + cv2.CALIB_CB_FAST_CHECK +
                cv2.CALIB_CB_NORMALIZE_IMAGE)

            if ret == True:
                corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
                img = cv2.drawChessboardCorners(img, (7, 7), corners2, ret)
                if addAll:
                    objpoints.append(objp)
                    imgpoints.append(corners2)
                    continue
                cv2.imshow('img', img)
                print("Found corners do you want to use this photo y/n to delete press d  to add all press a top quit q :  ")
                key = cv2.waitKey(0)
                if key == ord('y'):
                    objpoints.append(objp)
                    imgpoints.append(corners2)
                elif key == ord('d'):
                    print("Removing image ")
                    os.remove(fname)
                elif key == ord('a'):
                    print("Adding all calibration pictures from folder")
                    addAll = True
                    cv2.destroyAllWindows()
                elif key == ord('s'):
                    continue
                elif key == ord('q'):
                    break
            else:
                print("No corners found deleting image!")
                os.remove(fname)
        cv2.destroyAllWindows()
        rms, camera_matrix, dist_coefs, _rvecs, _tvecs = cv2.calibrateCamera(objpoints, imgpoints, (w, h), None, None)

        config["CALIBRATION"]["cameraMatrix"] = str(camera_matrix)
        config["CALIBRATION"]["distortion"] = str(dist_coefs)
        print("\nRMS:", rms)
        print("camera matrix:\n", camera_matrix)
        print("distortion coefficients: ", dist_coefs.ravel())
        with open('source/config.ini', 'w') as configfile:
            config.write(configfile)
        cv2.destroyAllWindows()


if __name__ == '__main__':
    cal = CameraCalibration(False)
    cal.calibrate()
