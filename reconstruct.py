import copy
import glob
import os
import time
from configparser import ConfigParser

import cv2
import numpy as np
import open3d as o3d

DEBUG = True
target_width = 600
isFastRec =False;

class Scene3D():
    def __init__(self, cameraMatrix, distortion, img_path=None):
        self.img_path = img_path
        self.K = cameraMatrix
        self.K_inv = np.linalg.inv(cameraMatrix)
        self.d = distortion
        self.image_data, self.matches_data, errors = {}, {}, {}
        self.point_cloud = np.zeros((0, 3))
        self.baseRt = np.hstack((np.eye(3), np.zeros((3, 1))))
        self.point_clouds = []
        self.cloudDir = "clouds/"
        self.mergedCloud = None;

    def findKeypoints(self, img1, img2):
        sift = cv2.xfeatures2d.SIFT_create()

        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(
            cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY), None)
        kp2, des2 = sift.detectAndCompute(
            cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY), None)
        outimg = img1;
        # Find point matches
        FLANN_INDEX_KDTREE = 1
        index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
        search_params = dict(checks=50)
        flann = cv2.FlannBasedMatcher(index_params, search_params)

        matches = flann.knnMatch(des1, des2, k=2)
        # print("NO OF MATCHES SIFT : " + str(len(matches)))
        # Apply Lowe's SIFT matching ratio test
        good = []

        for m, n in matches:
            if m.distance < 0.65 * n.distance:
                good.append(m)
        print("NO OF GOOD 0.7 MATCHES : " + str(len(good)))
        # TODO to be deleted ?
        # img1idx = np.array([m.queryIdx for m in good])
        # img2idx = np.array([m.trainIdx for m in good])

        src_pts = np.asarray([kp1[m.queryIdx].pt for m in good])
        dst_pts = np.asarray([kp2[m.trainIdx].pt for m in good])
        # print("NO OF dst: " + str(len(dst_pts)))
        # print("NO OF src: " + str(len(src_pts)))
        # # TODO to be deleted !
        # flann_matches = cv2.drawMatches(img1, kp1, img2, kp2, good, None, flags=2)
        # cv2.imshow('name', flann_matches)
        # cv2.waitKey(0)
        return kp1, kp2, src_pts, dst_pts, good

    def poseEstimation(self, img1, img2):
        image1 = self.load_image(img1)
        image2 = self.load_image(img2)
        kp1, kp2, img1pts, img2pts, matches = self.findKeypoints(image1, image2)
        if len(img1pts) < len(self.match_pts1):
            #    print("using Optic Flow")
            img1pts = self.match_pts1
            img2pts = self.match_pts2
        if len(img1pts) < 220:
            print("no of matches is to low " + str(len(img1pts)))
            return False
        F, mask = cv2.findFundamentalMat(img1pts, img2pts, cv2.FM_RANSAC, 0.1, 0.99)
        E = self.K.T.dot(F).dot(self.K)

        # _, R, t, good = cv2.recoverPose(E, img1pts[mask], img2pts[mask], self.K)
        Rt = self._find_camera_matrices(img1pts, img2pts, mask, E)

        first_inliers = np.array(self.match_inliers1)[:, :2]
        second_inliers = np.array(self.match_inliers2)[:, :2]

        pts4D = cv2.triangulatePoints(self.baseRt, Rt, first_inliers.T,
                                      second_inliers.T).T
        pts3d = pts4D[:, :3] / pts4D[:, 3, None]

        self.last_pc = pts3d

        visualizer = o3d.visualization.VisualizerWithEditing()
        visualizer.create_window()
        renderOptions = visualizer.get_render_option()
        renderOptions.background_color = np.asarray([0, 0, 0])
        renderOptions.point_size = 2
        renderOptions.show_coordinate_frame = True
        a_cloud = o3d.geometry.PointCloud()
        a_cloud.points = o3d.utility.Vector3dVector(pts3d)
        visualizer.add_geometry(a_cloud)
        visualizer.run()
        visualizer.destroy_window()

        return True

    def _extract_keypoints_flow(self, image1, image2):
        img1 = self.load_image(image1)
        img2 = self.load_image(image2)

        fast = cv2.FastFeatureDetector_create()

        first_key_points = fast.detect(img1, None)

        first_key_list = [i.pt for i in first_key_points]
        first_key_arr = np.array(first_key_list).astype(np.float32)
        out_key_arr = np.array(first_key_list).astype(np.float32)
        second_key_arr, status, err = cv2.calcOpticalFlowPyrLK(img1, img2, first_key_arr, out_key_arr)

        condition = (status == 1) * (err < 4.)
        concat = np.concatenate((condition, condition), axis=1)
        first_match_points = first_key_arr[concat].reshape(-1, 2)
        second_match_points = second_key_arr[concat].reshape(-1, 2)
        self.match_pts1 = first_match_points
        self.match_pts2 = second_match_points

    # print("OPTIC matches " + str(len(self.match_pts1)))

    def plot_optic_flow(self, img1):
        img = self.load_image(img1)
        for i in range(len(self.match_pts1)):
            cv2.line(img, tuple(self.match_pts1[i]), tuple(self.match_pts2[i]), color=(255, 0, 0))
            theta = np.arctan2(self.match_pts2[i][1] - self.match_pts1[i][1], self.match_pts2[i][0] -
                               self.match_pts1[i][0])
            cv2.line(img, tuple(self.match_pts2[i]),
                     (np.int(self.match_pts2[i][0] - 6 * np.cos(theta + np.pi / 4)),
                      np.int(self.match_pts2[i][1] -
                             6 * np.sin(theta + np.pi / 4))), color=(255, 0, 0))
            cv2.line(img, tuple(self.match_pts2[i]),
                     (np.int(self.match_pts2[i][0] - 6 * np.cos(theta - np.pi / 4)), np.int(self.match_pts2[i][1] -
                                                                                            6 * np.sin(
                         theta - np.pi / 4))), color=(255, 0, 0))
            for i in range(len(self.match_pts1)):
                cv2.line(img, tuple(self.match_pts1[i]), tuple(self.match_pts2[i]), color=(255, 0, 0))
                theta = np.arctan2(self.match_pts2[i][1] - self.match_pts1[i][1],
                                   self.match_pts2[i][0] - self.match_pts1[i][0])

    def _find_camera_matrices(self, src_pts, dst_pts, Fmask, E):
        U, S, Vt = np.linalg.svd(E)
        W = np.array([0.0, -1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]).reshape(3, 3)
        first_inliers = []
        second_inliers = []
        for i in range(len(Fmask)):
            if Fmask[i]:
                first_inliers.append(self.K_inv.dot([src_pts[i][0], src_pts[i][1], 1.0]))
                second_inliers.append(self.K_inv.dot([dst_pts[i][0], dst_pts[i][1], 1.0]))

        R = U.dot(W).dot(Vt)
        T = U[:, 2]

        if not self._in_front_of_both_cameras(first_inliers, second_inliers, R, T):
            # Second choice: R = U * W * Vt, T = -u_3
            T = - U[:, 2]

        if not self._in_front_of_both_cameras(first_inliers, second_inliers, R, T):
            # Third choice: R = U * Wt * Vt, T = u_3
            R = U.dot(W.T).dot(Vt)
            T = U[:, 2]

        if not self._in_front_of_both_cameras(first_inliers, second_inliers, R, T):
            # Fourth choice: R = U * Wt * Vt, T = -u_3
            T = - U[:, 2]

        self.match_inliers1 = first_inliers
        self.match_inliers2 = second_inliers
        # self.Rt1 = np.hstack((np.eye(3), np.zeros((3, 1))))
        # self.Rt2 = np.hstack((R, T.reshape(3, 1)))
        return np.hstack((R, T.reshape(3, 1)))

    def _find_camera_matrices_rt(self, src_pts, dst_pts, Fmask, E):

        """Finds the [R|t] camera matrix"""
        # decompose essential matrix into R, t (See Hartley and Zisserman 9.13)
        U, S, Vt = np.linalg.svd(E)
        W = np.array([0.0, -1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                      1.0]).reshape(3, 3)

        # iterate over all point correspondences used in the estimation of the
        # fundamental matrix
        first_inliers = []
        second_inliers = []
        for pt1, pt2, mask in zip(
                src_pts, dst_pts, Fmask):
            if mask:
                # normalize and homogenize the image coordinates
                first_inliers.append(self.K_inv.dot([pt1[0], pt1[1], 1.0]))
                second_inliers.append(self.K_inv.dot([pt2[0], pt2[1], 1.0]))

        # Determine the correct choice of second camera matrix
        # only in one of the four configurations will all the points be in
        # front of both cameras
        R = T = None
        R = U.dot(W.T).dot(Vt)
        T = U[:, 2]
        for r in (U.dot(W).dot(Vt), U.dot(W.T).dot(Vt)):
            for t in (U[:, 2], -U[:, 2]):
                if self._in_front_of_both_cameras(
                        first_inliers, second_inliers, r, t):
                    R, T = r, t

        assert R is not None, "Camera matricies were never found"

        self.match_inliers1 = first_inliers
        self.match_inliers2 = second_inliers

        Rt2 = np.hstack((R, T.reshape(3, 1)))
        return Rt2

    def _in_front_of_both_cameras(self, first_points, second_points, rot,
                                  trans):

        for first, second in zip(first_points, second_points):
            first_z = np.dot(rot[0, :] - second[0] * rot[2, :],
                             trans) / np.dot(rot[0, :] - second[0] * rot[2, :],
                                             second)
            first_3d_point = np.array([first[0] * first_z,
                                       second[0] * first_z, first_z])
            second_3d_point = np.dot(rot.T, first_3d_point) - np.dot(rot.T,
                                                                     trans)
            if first_3d_point[2] < 0 or second_3d_point[2] < 0:
                return False

        return True

    def load_image(self, img_path: str, ):
        """Loads pair of images

            This method loads the two images for which the 3D scene should be
            reconstructed. The two images should show the same real-world scene
            from two different viewpoints.

            :param img_path1: path to first image
            :param img_path2: path to second image
            :param use_pyr_down: flag whether to downscale the images to
                                 roughly 600px width (True) or not (False)
        """

        img = cv2.imread(img_path, cv2.CV_8UC3)

        # make sure image is valid
        assert img is not None, f"Image {img_path} could not be loaded."
        img = cv2.undistort(cv2.cvtColor(img, cv2.COLOR_GRAY2BGR), self.K, self.d)

        # scale down image if necessary
        while img.shape[1] > 2 * target_width:
            img = cv2.pyrDown(img)

        return img

    def globalreg(self, pc1, pc2):
        def draw_registration_result(source, target, transformation):
            source_temp = copy.deepcopy(source)
            target_temp = copy.deepcopy(target)
            source_temp.transform(transformation)
            mergedCP = source_temp + target_temp
            source_temp.paint_uniform_color([1, 0.706, 0])
            target_temp.paint_uniform_color([0, 0.651, 0.929])
            o3d.visualization.draw_geometries([source_temp, target_temp])
            return mergedCP

        def preprocess_point_cloud(pcd, voxel_size):
            print(":: Downsample with a voxel size %.3f." % voxel_size)
            pcd_down = pcd.voxel_down_sample(voxel_size)

            radius_normal = voxel_size * 2
            print(":: Estimate normal with search radius %.3f." % radius_normal)
            pcd_down.estimate_normals(
                o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))

            radius_feature = voxel_size * 5
            print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
            pcd_fpfh = o3d.pipelines.registration.compute_fpfh_feature(
                pcd_down,
                o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))
            return pcd_down, pcd_fpfh

        def prepare_dataset(voxel_size):
            print(":: Load two point clouds and disturb initial pose.")
            source = pc1
            target = pc2

            # draw_registration_result(source, target, np.identity(4))

            source_down, source_fpfh = preprocess_point_cloud(source, voxel_size)
            target_down, target_fpfh = preprocess_point_cloud(target, voxel_size)
            return source, target, source_down, target_down, source_fpfh, target_fpfh

        def execute_global_registration(source_down, target_down, source_fpfh,
                                        target_fpfh, voxel_size):
            distance_threshold = voxel_size * 1.5
            print(":: RANSAC registration on downsampled point clouds.")
            print("   Since the downsampling voxel size is %.3f," % voxel_size)
            print("   we use a liberal distance threshold %.3f." % distance_threshold)
            result = o3d.pipelines.registration.registration_ransac_based_on_feature_matching(
                source_down, target_down, source_fpfh, target_fpfh, distance_threshold,
                o3d.pipelines.registration.TransformationEstimationPointToPoint(False), 4, [
                    o3d.pipelines.registration.CorrespondenceCheckerBasedOnEdgeLength(0.9),
                    o3d.pipelines.registration.CorrespondenceCheckerBasedOnDistance(
                        distance_threshold)
                ], o3d.pipelines.registration.RANSACConvergenceCriteria(4000000, 500))
            return result

        def refine_registration(source, target, source_fpfh, target_fpfh, voxel_size, ransac_res):
            distance_threshold = voxel_size * 0.4
            print(":: Point-to-plane ICP registration is applied on original point")
            print("   clouds to refine the alignment. This time we use a strict")
            print("   distance threshold %.3f." % distance_threshold)
            result = o3d.pipelines.registration.registration_icp(
                source, target, distance_threshold, ransac_res.transformation,
                o3d.pipelines.registration.TransformationEstimationPointToPoint())
            return result

        voxel_size = 0.22
        source, target, source_down, target_down, source_fpfh, target_fpfh = \
            prepare_dataset(voxel_size)

        result_ransac = execute_global_registration(source_down, target_down,
                                                    source_fpfh, target_fpfh,
                                                    voxel_size)

        result_icp = refine_registration(source, target, source_fpfh, target_fpfh,
                                         voxel_size, result_ransac)

        cloud = draw_registration_result(source, target, result_icp.transformation)
        var = input("to rerun registartion type r , to quit without saving q , to save type anything else ")

        if var == 'q':
            if (self.mergedCloud == None):
                self.mergedCloud = source
            return
        elif var == 'r':
            self.globalreg(pc1, pc2)
        else:
            self.mergedCloud = cloud

    def fastRec(self, i, files):
        self._extract_keypoints_flow(files[i], files[i+1])
        if (False == self.poseEstimation(files[i], files[i+1])):
            return
        else:
            a_cloudd = o3d.geometry.PointCloud()
            a_cloudd.points = o3d.utility.Vector3dVector(self.last_pc)
            o3d.io.write_point_cloud(self.cloudDir + 'ptcld' + str(i) + "_" + str(i+1) + '.ply',
                                     a_cloudd)
            self.point_clouds.append(self.last_pc)

    def longRec(self, i, files,noOfFiles):
        for j in range(i + 1, noOfFiles):
            self._extract_keypoints_flow(files[i], files[j])
            if (False == self.poseEstimation(files[i], files[j])):
                continue
            else:
                var = input("pres s to save PC or q to exit ")

                if (var == 's'):
                    a_cloudd = o3d.geometry.PointCloud()
                    a_cloudd.points = o3d.utility.Vector3dVector(self.last_pc)
                    o3d.io.write_point_cloud(self.cloudDir + 'ptcld' + str(i) + "_" + str(j) + '.ply',
                                             a_cloudd)
                    self.point_clouds.append(self.last_pc)
                elif (var == 'q'):
                    break

    def reconstructScene(self):
        files = glob.glob(self.img_path + '*')
        noOfFiles = len(files)
        var = input("do you want to skip matching and gen pc ? " + str(noOfFiles))
        couter = 0
        if var == 'n':

            time_start = time.time()
            if (True):
                self.cloudDir = "clouds/" + time.strftime("%Y%m%d-%H%M%S" + "/")
                os.mkdir(self.cloudDir)
                for i in range(0, noOfFiles - 1):
                    if(isFastRec):
                        self.fastRec(i,files)
                    else:
                        self.longRec(i,files,noOfFiles)

            else:
                for i in range(1, noOfFiles):
                    self._extract_keypoints_flow(files[i - 1], files[i])
                    self.poseEstimation(files[i - 1], files[i])
                    var = input("pres s to save PC or q to exit ")
                    if (var == 's'):
                        a_cloudd = o3d.geometry.PointCloud()
                        a_cloudd.points = o3d.utility.Vector3dVector(self.last_pc)
                        o3d.io.write_point_cloud(cloudDir + str(i) + '.ply', a_cloudd)
                        self.point_clouds.append(self.last_pc)
                    elif (var == 'q'):
                        break
        print("Execution time " + str((time.time() - time_start)))
        clouds = glob.glob(self.cloudDir + 'ptcld' + '*')
        cloudsNo = len(clouds)
        print("Number of generated Clouds " + str(cloudsNo))
        print("Number of comparisions " + str(couter))

        print('ptcld dir ' + self.cloudDir)

        if (cloudsNo <= 0):
            print("no ptcld in dir " + self.cloudDir)
            return
        cl1 = o3d.io.read_point_cloud(clouds[0])
        cl2 = o3d.io.read_point_cloud(clouds[1])
        self.globalreg(cl1, cl2)
        for i in range(2, cloudsNo):
            cloud = o3d.io.read_point_cloud(clouds[i])
            self.globalreg(cloud, self.mergedCloud)

        o3d.io.write_point_cloud(self.cloudDir + 'finalMerged' + str(i) + '.ply', self.mergedCloud)

        visualizer = o3d.visualization.VisualizerWithEditing()
        visualizer.create_window()
        renderOptions = visualizer.get_render_option()
        renderOptions.background_color = np.asarray([0, 0, 0])
        renderOptions.point_size = 3
        renderOptions.show_coordinate_frame = True

        visualizer.add_geometry(self.mergedCloud)
        visualizer.run()
        visualizer.destroy_window()


def takeShotsOfScene():
    WELCOME_STRIG = 'To take scene picture press f and press enter\n To quit press q'
    iterator = 0
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280.0)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1024.0)

    while cap.isOpened():
        ret, frame = cap.read()
        if ret == True:
            gray = frame
            cv2.imshow('Colecting photo set', gray)
            key = cv2.waitKey(1) & 0xFF
            if key == ord('f'):
                iterator += 1
                cv2.imwrite('source/scene_fl/' + str(iterator) + '_' + '.jpg',
                            gray)
                print('Taken ' + str(iterator) + ' photo \n' + WELCOME_STRIG)
            elif key == ord('q'):
                cap.release()
                cv2.destroyAllWindows()
                break
    cap.release()
    cv2.destroyAllWindows()


def reconstructFromPhotoSet(K=None, d=None):
    config = ConfigParser()
    config.read("source/config.ini")

    if K == None:
        #K = np.array([[2759.48 / 4, 0, 1520.69 / 4, 0, 2764.16 / 4, 1006.81 / 4, 0, 0, 1]]).reshape(3, 3)
        K = np.array([[799.62168463, 0,  340.83723232, 0, 797.917152, 246.39839983, 0, 0, 1]]).reshape(3, 3)
        # K = np.array([[2759.48, 0, 1520.69, 0, 2764.16, 1006.81, 0, 0, 1]]).reshape(3, 3)
        # K = np.array([[4.28643058e+03 , 0, 1.66128788e+03, 0,5.72435708e+03, 2.61391816e+03, 0, 0, 1]]).reshape(3, 3) tele
        # K = np.array([[799.62168463 , 0,  340.83723232, 0, 797.917152,
        #              246.39839983, 0, 0, 1]]).reshape(3, 3)
    if d == None:
        #d = np.array([0, 0, 0, 0, 0]).reshape(1, 5)
        d = np.array([-5.79406010e-02, 9.91643211e-01, 9.13019696e-04,  5.53088024e-04, -2.88896167e+00]).reshape(1, 5)
        # d = np.array([-0.16442158, - 4.05158311, - 0.08910871, - 0.18166819,  6.17214107]).reshape(1, 5) tele

    print(K)

    scene = Scene3D(K, d, img_path='source/scene_fl/')
    scene.reconstructScene()


def main():
    # K = np.array([[592.46678039/2 , 0,  318.30116819/2, 0, 590.84439827/2,
    #               235.08394588/2, 0, 0, 1]]).reshape(3, 3)

    K = np.array([[803.48819716, 0, 342.55956532, 0, 801.98622431,
                   241.85462859, 0, 0, 1]]).reshape(3, 3)
    d = np.array([-5.90659055e-02, 1.09478097e+00, -1.32402583e-03, 1.13862982e-03, -3.40233463e+00]).reshape(1, 5)

    scene = Scene3D(K, d, img_path='scene/bench/')
    scene.reconstructScene()


# scene.poseEstimation("scene/t/0000.png", "scene/t/0001.png")


if __name__ == '__main__':
    main()
