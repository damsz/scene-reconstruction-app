from calibration import CameraCalibration
from reconstruct import takeShotsOfScene
from reconstruct import reconstructFromPhotoSet

def cameraCalib():
    calib = CameraCalibration(True,0)
    calib.calibrate()
    return


def main_menu():
    while True:
        var = input(
            "This is Scene reconstruction program.\n To Calibrate camera press 1 "
            "\n To take photos of Scene press 2 "
            "\n To Reconstruct Scene from photo set press 3  "
            "\n To exit press q \n")
        if var == '1':
            cameraCalib()
        elif var == '2':
            takeShotsOfScene()
        elif var == '3':
            reconstructFromPhotoSet()
        elif var == 'q':
            break


if __name__ == '__main__':
    main_menu()

